package org.afox.formatting;

/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * This exception is thrown whenever there is a formatting problem.  The
 * message indicates what the problem is
 *
 * @author <A HREF="mailto:schock@afox.org">Craig Schock</A>
 * @version 1.0
 */

public class FormatException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = -285259416966656413L;

    public FormatException(String aMessage) {
        super(aMessage);
    }
}
