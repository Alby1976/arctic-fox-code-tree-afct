/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package org.afox.util.validators.string;

import org.afox.util.validators.*;

public class MaxLengthValidator extends Validator {
    private int maxLength = 0;

    public MaxLengthValidator(int aMaxLength) {
        super();
        maxLength = aMaxLength;
    }

    public MaxLengthValidator(int aMaxLength, String errorMessage) {
        this(null, aMaxLength, errorMessage);
    }

    public MaxLengthValidator(Validator previous, int aMaxLength) {
        super(previous);
        maxLength = aMaxLength;
    }

    public MaxLengthValidator(Validator previous, int aMaxLength, String errorMessage) {
        super(previous, errorMessage);
        maxLength = aMaxLength;
    }

    protected void validateImpl(Object theParameter) throws ValidateException {
        String target = (String) theParameter;
        if (target.length() > maxLength)
            throw new ValidateException(getErrorMessage("Max Length (" + maxLength +
                    ") exceeded: " + "\"" + target +
                    "\" (" + target.length() + ")"));
    }

    protected String getDescription() {
        return "String length cannot exceed " + maxLength + " charcters.";
    }
}
