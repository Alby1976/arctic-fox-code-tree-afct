/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package org.afox.util.validators.string;

import org.afox.util.validators.*;

public class SequenceValidator extends Validator {
    private String validSequence;

    public SequenceValidator(String sequence) {
        super();
        validSequence = sequence;
    }

    public SequenceValidator(String sequence, String errorMessage) {
        this(null, sequence, errorMessage);
    }

    public SequenceValidator(Validator previous, String sequence) {
        super(previous);
        validSequence = sequence;
    }

    public SequenceValidator(Validator previous, String sequence, String errorMessage) {
        super(previous, errorMessage);
        validSequence = sequence;
    }

    protected void validateImpl(Object theParameter) throws ValidateException {
        String target = (String) theParameter;
        int index = 0;
        for (int i = 0; i < validSequence.length(); i++) {
            try {
                index = target.indexOf(validSequence.charAt(i), index);
                if (index == (-1))
                    throw new ValidateException(getErrorMessage("Valid sequence(" +
                            validSequence +
                            ") not found."));
                index++;
            } catch (Exception x) {
                throw new ValidateException(getErrorMessage("Valid sequence(" +
                        validSequence +
                        ") not found."));
            }
        }

    }

    protected String getDescription() {
        return "Valid strings must contain the following characters in the specified order: \"" +
                validSequence + "\".";
    }
}
