/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package org.afox.util.validators.string;

import org.afox.util.validators.*;

public class InvalidCharValidator extends Validator {
    private String invalidCharacters;

    public InvalidCharValidator(String invalid) {
        super();
        invalidCharacters = invalid;
    }

    public InvalidCharValidator(String invalid, String errorMessage) {
        this(null, invalid, errorMessage);
    }

    public InvalidCharValidator(Validator previous, String invalid) {
        super(previous);
        invalidCharacters = invalid;
    }

    public InvalidCharValidator(Validator previous, String invalid, String errorMessage) {
        super(previous, errorMessage);
        invalidCharacters = invalid;
    }

    protected void validateImpl(Object theParameter) throws ValidateException {
        String target = (String) theParameter;
        for (int i = 0; i < invalidCharacters.length(); i++) {
            if (target.indexOf(invalidCharacters.charAt(i)) != (-1))
                throw new ValidateException(getErrorMessage("Invalid character(" +
                        invalidCharacters.charAt(i) + ") " +
                        "found in String: \"" + target + "\""));
        }
    }

    protected String getDescription() {
        return "The following characters are not valid:" + invalidCharacters;
    }
}
