/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package org.afox.util.validators.string;

import org.afox.util.validators.*;

public class MinLengthValidator extends Validator {
    private int minLength = 0;

    public MinLengthValidator(int aMinLength) {
        this(null, aMinLength);
    }

    public MinLengthValidator(int aMinLength, String errorMessage) {
        this(null, aMinLength, errorMessage);
    }

    public MinLengthValidator(Validator previous, int aMinLength) {
        super(previous);
        setLength(aMinLength);
    }

    public MinLengthValidator(Validator previous, int aMinLength, String errorMessage) {
        super(previous, errorMessage);
        setLength(aMinLength);
    }

    private void setLength(int aValue) {
        if (aValue < 0)
            throw new IllegalArgumentException("Length must be > 0");
        minLength = aValue;
    }

    protected void validateImpl(Object theParameter) throws ValidateException {
        String target = (String) theParameter;
        if (target.length() < minLength)
            throw new ValidateException(getErrorMessage("Min Length (" +
                    minLength + ") not met: " +
                    "\"" + target + "\" (" +
                    target.length() + ")"));
    }

    protected String getDescription() {
        return "Strings must be a minimum of " + minLength + " characters in length.";
    }

}
