/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package org.afox.util.validators.string;

import org.afox.util.validators.*;

public class ValidCharRangeValidator extends Validator {
    private int lowRange;
    private int highRange;

    public ValidCharRangeValidator(int low, int high) {
        this(null, low, high);
    }

    public ValidCharRangeValidator(int low, int high, String errorMessage) {
        this(null, low, high, errorMessage);
    }

    public ValidCharRangeValidator(Validator previous, int low, int high) {
        super(previous);
        lowRange = low;
        highRange = high;
    }

    public ValidCharRangeValidator(Validator previous, int low, int high, String errorMessage) {
        super(previous, errorMessage);
        lowRange = low;
        highRange = high;
    }

    protected void validateImpl(Object theParameter) throws ValidateException {
        String target = (String) theParameter;
        for (int i = 0; i < target.length(); i++) {
            if ((target.charAt(i) < lowRange) || (target.charAt(i) > highRange))
                throw new ValidateException(getErrorMessage("Character(" +
                        target.charAt(i) +
                        ") out of range."));
        }
    }

    protected String getDescription() {
        return "All characters must be within the range: " + lowRange + " - " + highRange;
    }

}
