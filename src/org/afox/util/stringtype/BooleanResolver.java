package org.afox.util.stringtype;

public class BooleanResolver extends StringResolver {
    public Object parseImpl(String data) {
        if (data.equals("true"))
            return Boolean.TRUE;
        else if (data.equals("false"))
            return Boolean.FALSE;
        else
            return null;
    }

}
