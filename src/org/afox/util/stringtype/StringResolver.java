package org.afox.util.stringtype;

public abstract class StringResolver {
    private StringResolver next = null;

    public void add(StringResolver aResolver) {
        if (next == null)
            next = aResolver;
        else
            next.add(aResolver);
    }

    public Object parse(String data) {
        if (data == null)
            return null;

        Object anObject = parseImpl(data);
        if (anObject != null)
            return anObject;

        if (next == null)
            return data;
        else
            return next.parse(data);

    }

    public boolean verifyDigits(String data) {
        for (int i = 0; i < data.length(); i++) {
            if (!Character.isDigit(data.charAt(i)))
                return false;
        }
        return true;
    }

    public abstract Object parseImpl(String data);

}
