package org.afox.util.stringtype;

import org.afox.util.*;

public class VariableResolver extends StringResolver {
    public Object parseImpl(String data) {
//		System.out.println("Variable Resolver:" + data);
        try {
            if (data.startsWith("$")) {
                return Variable.get(data.substring(1));
            }
        } catch (NumberFormatException x) {
            // do nothing
        }
        return null;
    }

}
