package org.afox.util.stringtype;

public class StringQuoteResolver extends StringResolver {
    public Object parseImpl(String data) {
        try {
            if (data.charAt(0) == '"' && data.charAt(data.length() - 1) == '"')
                return data.substring(1, data.length() - 2);
        } catch (NumberFormatException x) {
            // do nothing
        }
        return null;
    }

}
