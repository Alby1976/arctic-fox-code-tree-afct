package org.afox.util.stringtype;

public class DoubleResolver extends StringResolver {
    public Object parseImpl(String data) {
        try {
            if (data.charAt(data.length() - 1) == 'D') {
                return Double.parseDouble(data.substring(0, data.length() - 1));
            }
        } catch (NumberFormatException x) {
            // do nothing
        }
        return null;
    }

}
