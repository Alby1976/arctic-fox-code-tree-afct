package org.afox.util.stringtype;

public class FloatResolver extends StringResolver {
    public Object parseImpl(String data) {
        try {
            if (data.charAt(data.length() - 1) == 'F') {
                return Float.parseFloat(data.substring(0, data.length() - 1));
            }
        } catch (NumberFormatException x) {
            // do nothing
        }
        return null;
    }

}
