package org.afox.util.stringtype;

public class LongResolver extends StringResolver {
    public Object parseImpl(String data) {
        try {
            if (data.charAt(data.length() - 1) == 'L') {
                return Long.parseLong(data.substring(0, data.length() - 1));
            }
        } catch (NumberFormatException x) {
            // do nothing
        }
        return null;
    }

}
