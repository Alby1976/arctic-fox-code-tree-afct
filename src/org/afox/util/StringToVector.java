/*  
    This file is part of the Arctic Fox Code Tree (AFCT).
    Copyright 2002, Arctic Fox Computer Consulting Inc.

    AFCT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    AFCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AFCT; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package org.afox.util;

import java.util.*;

/**
 * This class converts a String to a Vector of Strings (or Longs or Integers) based on
 * a separator character.  All characters between the separators are gathered and,
 * if necessary, converted to a Long or Integer and added to a resulting Vector.
 *
 * <P>If an attempt is made to create a vector of Longs or Integers and the data
 * contains non-integral data, an IllegalArgumentException is thrown.
 */

@SuppressWarnings({"rawtypes", "unchecked"})
public class StringToVector {
    private static final String DEFAULT_SEPARATOR = ",";
    private String theData;
    private int index;
    private int index1;
    private String separator;

    public StringToVector(String data) {
        this(data, DEFAULT_SEPARATOR);
    }

    public StringToVector(String data, String aSeparator) {
        theData = data;
        setSeparator(aSeparator);
    }

    public void setSeparator(String aSeparator) {
        separator = aSeparator;
    }

    public String getSeparator() {
        return separator;
    }

    /**
     * This Method parses the data string and returns a Vector of Strings which
     * represent the original data
     */
    public Vector parse() {
        Vector returnVector = new Vector();
        index = 0;
        index1 = -1;

        String temp = null;
        while ((temp = getNext()) != null) {
            returnVector.addElement(temp);
        }

        return returnVector;
    }

    /**
     * This Method parses the data string and returns a Vector of Longs which
     * represent the original data
     */
    public Vector parseLong() {
        Vector returnVector = new Vector();
        index = 0;
        index1 = -1;

        String temp = null;
        while ((temp = getNext()) != null) {
            try {
                returnVector.addElement(Long.parseLong(temp));
            } catch (Exception x) {
                throw new IllegalArgumentException("Vector contained non-long value");
            }
        }

        return returnVector;
    }

    /**
     * This Method parses the data string and returns a Vector of Integers which
     * represent the original data
     */
    public Vector parseInt() {
        Vector returnVector = new Vector();
        index = 0;
        index1 = -1;

        String temp = null;
        while ((temp = getNext()) != null) {
            try {
                returnVector.addElement(Integer.parseInt(temp));
            } catch (Exception x) {
                throw new IllegalArgumentException("Vector contained non-integer value");
            }
        }

        return returnVector;
    }

    private String getNext() {
        index1 = theData.indexOf(separator, index);

        if (index == -1)
            return null;
        else if (index1 == -1) {
            String temp = theData.substring(index);
            index = -1;
            return temp;
        }

        String temp = theData.substring(index, index1);
        index = index1 + separator.length();
        return temp;
    }

    public static void main(String[] args) {
        StringToVector aParse = new StringToVector(args[0]);

        Vector temp = aParse.parse();
        System.out.println("Results:" + temp);
    }
}
