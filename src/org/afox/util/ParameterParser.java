package org.afox.util;

import java.util.*;

/**
 * NOTE: The design of this class is very weak.  I needed to get something working
 * in order to get the other parts of this application up and running quickly.
 * I really need to re-think the parsing of parameters once all of the syntax
 * issues have been identified -crs
 */

@SuppressWarnings({"rawtypes", "unchecked"})
public class ParameterParser {
    private String separator = " ";
    private int index;

    public ParameterParser() {
    }

    public ParameterParser(String aSeparator) {
        separator = aSeparator;
    }

    public List parse(String line) {
        ArrayList aList = new ArrayList();
        index = 0;
        while (index < line.length()) {
            aList.add(StringType.parse(nextToken(line)));
        }
        return aList;
    }

    private String nextToken(String line) {
        StringBuffer sb = new StringBuffer();
        while (index < line.length() && Character.isWhitespace(line.charAt(index))) index++;
        while (index < line.length()) {
            if (isSeparator(line.charAt(index))) {
                break;
            }
            if (isList(line.charAt(index))) {
//				return parseList();
            }
            if (line.charAt(index) == '"') {
                index++;
                while (index < line.length()) {
                    if (line.charAt(index) == '"') {
                        index++;
                        return sb.toString();
                    }
                    sb.append(line.charAt(index++));
                }
            } else
                sb.append(line.charAt(index++));
        }
        index++;
        return sb.toString();
    }

    private boolean isSeparator(char a) {
        return (a == separator.charAt(0));
    }

    private boolean isList(char a) {
        return (a == '{');
    }

}
