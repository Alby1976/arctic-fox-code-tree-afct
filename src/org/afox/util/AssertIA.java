package org.afox.util;

import org.afox.util.assertImpl.*;

public abstract class AssertIA {
    private static AssertIA current = new AssertIANormal();

    public static void setDebug(boolean aValue) {
        if (aValue)
            current = new AssertIADebug();
        else
            current = new AssertIANormal();
    }

    public static void assertTrue(boolean assertion, String errorMessage) {
        current.assertTrueImpl(assertion, errorMessage);
    }

    public static void assertFalse(boolean assertion, String errorMessage) {
        current.assertFalseImpl(assertion, errorMessage);
    }

    public abstract void assertTrueImpl(boolean assertion, String errorMessage);

    public abstract void assertFalseImpl(boolean assertion, String errorMessage);


}
