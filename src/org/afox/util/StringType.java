package org.afox.util;

import org.afox.util.stringtype.*;

public class StringType {
    public static StringType defaultInstance = null;

    private StringResolver head;

    private static StringType getInstance() {
        if (defaultInstance == null) {
            defaultInstance = new StringType();
        }
        return defaultInstance;
    }

    private StringType() {
        setupDefault();
    }

    private void setupDefault() {
        head = new LongResolver();
        head.add(new IntResolver());
        head.add(new DoubleResolver());
        head.add(new FloatResolver());
        head.add(new BooleanResolver());
        head.add(new VariableResolver());
    }

    public static Object parse(String data) {
        return getInstance().parseImpl(data);
    }

    private Object parseImpl(String data) {
        return head.parse(data);
    }
}
