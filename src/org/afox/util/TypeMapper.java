package org.afox.util;

import java.util.*;


@SuppressWarnings({"rawtypes", "unchecked"})
public class TypeMapper {
    private static HashMap types = new HashMap();

    static {
        types.put(int.class, Integer.class);
        types.put(long.class, Long.class);
        types.put(float.class, Float.class);
        types.put(double.class, Double.class);
        types.put(byte.class, Byte.class);
        types.put(short.class, Short.class);
        types.put(boolean.class, Boolean.class);
        types.put(char.class, Character.class);
    }

    public static boolean match(Class type1, Class type2) {
        if (type1.isPrimitive())
            return matchPrimitive(type1, type2);
        else
            return type1.equals(type2) || type1.isAssignableFrom(type2);
    }

    private static boolean matchPrimitive(Class type1, Class type2) {
        return types.get(type1).equals(type2);
    }

    public static boolean matchParameters(Class[] list1, Class[] list2) {
        if (list1.length != list2.length)
            return false;

        for (int i = 0; i < list1.length; i++) {
            if (!match(list1[i], list2[i]))
                return false;
        }
        return true;
    }
}
