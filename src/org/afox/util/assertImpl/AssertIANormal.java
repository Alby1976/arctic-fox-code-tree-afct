package org.afox.util.assertImpl;

import org.afox.util.*;

public class AssertIANormal extends AssertIA {
    public void assertTrueImpl(boolean assertion, String errorMessage) {
        if (!assertion)
            throw new IllegalArgumentException(errorMessage);
    }

    public void assertFalseImpl(boolean assertion, String errorMessage) {
        if (assertion)
            throw new IllegalArgumentException(errorMessage);
    }

}
