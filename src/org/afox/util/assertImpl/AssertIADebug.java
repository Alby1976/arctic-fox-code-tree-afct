package org.afox.util.assertImpl;

import org.afox.util.*;

public class AssertIADebug extends AssertIA {
    public void assertTrueImpl(boolean assertion, String errorMessage) {
        System.out.println("Assert True: " + assertion + " " + errorMessage);
        if (!assertion)
            throw new IllegalArgumentException(errorMessage);
    }

    public void assertFalseImpl(boolean assertion, String errorMessage) {
        System.out.println("Assert False: " + assertion + " " + errorMessage);
        if (assertion)
            throw new IllegalArgumentException(errorMessage);
    }

}
